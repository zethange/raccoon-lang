package parser

import (
	"fmt"
	"strconv"

	. "gitlab.com/zethange/raccoon/tokenizer"
)

type Error struct {
	Position Position
	Message  string
}

func (e Error) Error() string {
	return fmt.Sprintf("parse error at %d:%d: %s", e.Position.Line, e.Position.Column, e.Message)
}

type parser struct {
	tokenizer *Tokenizer
	pos       Position
	tok       Token
	val       string
}

func (p *parser) next() {
	p.pos, p.tok, p.val = p.tokenizer.Next()
	if p.tok == ILLEGAL {
		p.error("%s", p.val)
	}
}

func (p *parser) error(format string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	panic(Error{p.pos, message})
}

func (p *parser) expect(tok Token) {
	if p.tok != tok {
		p.error("expected %s and not %s", tok, p.tok)
	}
	p.next()
}

func (p *parser) matches(operators ...Token) bool {
	for _, operator := range operators {
		if p.tok == operator {
			return true
		}
	}
	return false
}

func (p *parser) program() *Program {
	statements := p.statements(EOF)
	return &Program{statements}
}

func (p *parser) statements(end Token) Block {
	statements := Block{}
	for p.tok != end && p.tok != EOF {
		statements = append(statements, p.statement())
	}
	return statements
}

func (p *parser) statement() Statement {
	switch p.tok {
	case IF:
		return p.if_()
	case YET:
		return p.while()
	case FOR:
		return p.for_()
	case RETURN:
		return p.return_()
	case FUNC:
		return p.func_()
	}
	pos := p.pos
	expr := p.expression()
	if p.tok == ASSIGN {
		pos = p.pos
		switch expr.(type) {
		case *Variable, *Subscript:
			p.next()
			value := p.expression()
			return &Assign{pos, expr, value}
		default:
			p.error("expected name, subscript, or dot expression on left side of =")
		}
	}
	return &ExpressionStatement{pos, expr}
}

func (p *parser) block() Block {
	p.expect(LBRACE)
	body := p.statements(RBRACE)
	p.expect(RBRACE)
	return body
}

func (p *parser) if_() Statement {
	pos := p.pos
	p.expect(IF)
	condition := p.expression()
	body := p.block()
	var elseBody Block
	if p.tok == ELSE {
		p.next()
		if p.tok == LBRACE {
			elseBody = p.block()
		} else if p.tok == IF {
			elseBody = Block{p.if_()}
		} else {
			p.error("expected { or if after else, not %s", p.tok)
		}
	}
	return &If{pos, condition, body, elseBody}
}

func (p *parser) while() Statement {
	pos := p.pos
	p.expect(YET)
	condition := p.expression()
	body := p.block()
	return &While{pos, condition, body}
}

func (p *parser) for_() Statement {
	pos := p.pos
	p.expect(FOR)
	name := p.val
	p.expect(NAME)
	p.expect(IN)
	iterable := p.expression()
	body := p.block()
	return &For{pos, name, iterable, body}
}

func (p *parser) return_() Statement {
	pos := p.pos
	p.expect(RETURN)
	result := p.expression()
	return &Return{pos, result}
}

func (p *parser) func_() Statement {
	pos := p.pos
	p.expect(FUNC)
	if p.tok == NAME {
		name := p.val
		p.next()
		params, ellipsis := p.params()
		body := p.block()
		return &FunctionDefinition{pos, name, params, ellipsis, body}
	} else {
		params, ellipsis := p.params()
		body := p.block()
		expr := &FunctionExpression{pos, params, ellipsis, body}
		return &ExpressionStatement{pos, expr}
	}
}

func (p *parser) params() ([]string, bool) {
	p.expect(LPAREN)
	params := []string{}
	gotComma := true
	gotEllipsis := false
	for p.tok != RPAREN && p.tok != EOF && !gotEllipsis {
		if !gotComma {
			p.error("expected , between parameters")
		}
		param := p.val
		p.expect(NAME)
		params = append(params, param)
		if p.tok == ELLIPSIS {
			gotEllipsis = true
			p.next()
		}
		if p.tok == COMMA {
			gotComma = true
			p.next()
		} else {
			gotComma = false
		}
	}
	if p.tok != RPAREN && gotEllipsis {
		p.error("can only have ... after last parameter")
	}
	p.expect(RPAREN)
	return params, gotEllipsis
}

func (p *parser) binary(parseFunc func() Expression, operators ...Token) Expression {
	expr := parseFunc()
	for p.matches(operators...) {
		op := p.tok
		pos := p.pos
		p.next()
		right := parseFunc()
		expr = &Binary{pos, expr, op, right}
	}
	return expr
}

func (p *parser) expression() Expression {
	return p.binary(p.and, OR)
}

func (p *parser) and() Expression {
	return p.binary(p.not, AND)
}

func (p *parser) not() Expression {
	if p.tok == NOT {
		pos := p.pos
		p.next()
		operand := p.not()
		return &Unary{pos, NOT, operand}
	}
	return p.equality()
}

func (p *parser) equality() Expression {
	return p.binary(p.comparison, EQUAL, NOTEQUAL)
}

func (p *parser) comparison() Expression {
	return p.binary(p.addition, LT, LTE, GT, GTE, IN)
}

func (p *parser) addition() Expression {
	return p.binary(p.multiply, PLUS, MINUS)
}

func (p *parser) multiply() Expression {
	return p.binary(p.negative, TIMES, DIVIDE, MODULO)
}

func (p *parser) negative() Expression {
	if p.tok == MINUS {
		pos := p.pos
		p.next()
		operand := p.negative()
		return &Unary{pos, MINUS, operand}
	}
	return p.call()
}

func (p *parser) call() Expression {
	expr := p.primary()
	for p.matches(LPAREN, LBRACKET, DOT) {
		if p.tok == LPAREN {
			pos := p.pos
			p.next()
			args := []Expression{}
			gotComma := true
			gotEllipsis := false
			for p.tok != RPAREN && p.tok != EOF && !gotEllipsis {
				if !gotComma {
					p.error("expected , between arguments")
				}
				arg := p.expression()
				args = append(args, arg)
				if p.tok == ELLIPSIS {
					gotEllipsis = true
					p.next()
				}
				if p.tok == COMMA {
					gotComma = true
					p.next()
				} else {
					gotComma = false
				}
			}
			if p.tok != RPAREN && gotEllipsis {
				p.error("can only have ... after last argument")
			}
			p.expect(RPAREN)
			expr = &Call{pos, expr, args, gotEllipsis}
		} else if p.tok == LBRACKET {
			pos := p.pos
			p.next()
			subscript := p.expression()
			p.expect(RBRACKET)
			expr = &Subscript{pos, expr, subscript}
		} else {
			pos := p.pos
			p.next()
			subscript := &Literal{p.pos, p.val}
			p.expect(NAME)
			expr = &Subscript{pos, expr, subscript}
		}
	}
	return expr
}

func (p *parser) primary() Expression {
	switch p.tok {
	case NAME:
		name := p.val
		pos := p.pos
		p.next()
		return &Variable{pos, name}
	case INT:
		val := p.val
		pos := p.pos
		p.next()
		n, err := strconv.Atoi(val)
		if err != nil {
			panic(fmt.Sprintf("tokenizer gave INT token that isn't an int: %s", val))
		}
		return &Literal{pos, n}
	case STR:
		val := p.val
		pos := p.pos
		p.next()
		return &Literal{pos, val}
	case TRUE:
		pos := p.pos
		p.next()
		return &Literal{pos, true}
	case FALSE:
		pos := p.pos
		p.next()
		return &Literal{pos, false}
	case NIL:
		pos := p.pos
		p.next()
		return &Literal{pos, nil}
	case LBRACKET:
		return p.list()
	case LBRACE:
		return p.map_()
	case FUNC:
		pos := p.pos
		p.next()
		args, ellipsis := p.params()
		body := p.block()
		return &FunctionExpression{pos, args, ellipsis, body}
	case LPAREN:
		p.next()
		expr := p.expression()
		p.expect(RPAREN)
		return expr
	default:
		p.error("expected expression, not %s", p.tok)
		return nil
	}
}

func (p *parser) list() Expression {
	pos := p.pos
	p.expect(LBRACKET)
	values := []Expression{}
	gotComma := true
	for p.tok != RBRACKET && p.tok != EOF {
		if !gotComma {
			p.error("expected , between list elements")
		}
		value := p.expression()
		values = append(values, value)
		if p.tok == COMMA {
			gotComma = true
			p.next()
		} else {
			gotComma = false
		}
	}
	p.expect(RBRACKET)
	return &List{pos, values}
}

func (p *parser) map_() Expression {
	pos := p.pos
	p.expect(LBRACE)
	items := []MapItem{}
	gotComma := true
	for p.tok != RBRACE && p.tok != EOF {
		if !gotComma {
			p.error("expected , between map items")
		}
		key := p.expression()
		p.expect(COLON)
		value := p.expression()
		items = append(items, MapItem{key, value})
		if p.tok == COMMA {
			gotComma = true
			p.next()
		} else {
			gotComma = false
		}
	}
	p.expect(RBRACE)
	return &Map{pos, items}
}

func ParseExpression(input []byte) (e Expression, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(Error)
		}
	}()
	t := NewTokenizer(input)
	p := parser{tokenizer: t}
	p.next()
	return p.expression(), nil
}

func ParseProgram(input []byte) (prog *Program, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(Error)
		}
	}()
	t := NewTokenizer(input)
	p := parser{tokenizer: t}
	p.next()
	return p.program(), nil
}
