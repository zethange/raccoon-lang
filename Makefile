build:
	go build -ldflags="-s -w" ./raccoon.go 

build-wasm:
	GOOS=js GOARCH=wasm go build -ldflags="-s -w" -o public/raccoon.wasm ./raccoon.go
	cp $$(go env GOROOT)/misc/wasm/wasm_exec.js public/wasm_exec.js 
