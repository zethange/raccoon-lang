package interpreter

import (
	"fmt"

	. "gitlab.com/zethange/raccoon/tokenizer"
)

type Error interface {
	error
	Position() Position
}

type TypeError struct {
	Message string
	pos     Position
}

func (e TypeError) Error() string {
	return fmt.Sprintf("type error at %d:%d: %s", e.pos.Line, e.pos.Column, e.Message)
}

func (e TypeError) Position() Position {
	return e.pos
}

func typeError(pos Position, format string, args ...interface{}) error {
	return TypeError{fmt.Sprintf(format, args...), pos}
}

type ValueError struct {
	Message string
	pos     Position
}

func (e ValueError) Error() string {
	return fmt.Sprintf("value error at %d:%d: %s", e.pos.Line, e.pos.Column, e.Message)
}

func (e ValueError) Position() Position {
	return e.pos
}

func valueError(pos Position, format string, args ...interface{}) error {
	return ValueError{fmt.Sprintf(format, args...), pos}
}

type NameError struct {
	Message string
	pos     Position
}

func (e NameError) Error() string {
	return fmt.Sprintf("name error at %d:%d: %s", e.pos.Line, e.pos.Column, e.Message)
}

func (e NameError) Position() Position {
	return e.pos
}

func nameError(pos Position, format string, args ...interface{}) error {
	return NameError{fmt.Sprintf(format, args...), pos}
}

type RuntimeError struct {
	Message string
	pos     Position
}

func (e RuntimeError) Error() string {
	return fmt.Sprintf("runtime error at %d:%d: %s", e.pos.Line, e.pos.Column, e.Message)
}

func (e RuntimeError) Position() Position {
	return e.pos
}

func runtimeError(pos Position, format string, args ...interface{}) error {
	return RuntimeError{fmt.Sprintf(format, args...), pos}
}
