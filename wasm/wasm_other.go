//go:build !js && !wasm

package wasm

func Run() {
	panic("possible attempt to run wasm code on non-wasm platform")
}
