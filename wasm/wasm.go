//go:build js && wasm

package wasm

import (
	"bytes"
	"fmt"
	"strings"
	"syscall/js"

	"gitlab.com/zethange/raccoon/interpreter"
	"gitlab.com/zethange/raccoon/parser"
	"gitlab.com/zethange/raccoon/tokenizer"
)

func showErrorSource(source []byte, pos tokenizer.Position, dividerLen int) {
	divider := strings.Repeat("-", dividerLen)
	if divider != "" {
		fmt.Println(divider)
	}
	lines := bytes.Split(source, []byte{'\n'})
	errorLine := string(lines[pos.Line-1])
	numTabs := strings.Count(errorLine[:pos.Column-1], "\t")
	fmt.Println(strings.Replace(errorLine, "\t", "    ", -1))
	fmt.Println(strings.Repeat(" ", pos.Column-1) + strings.Repeat("   ", numTabs) + "^")
	if divider != "" {
		fmt.Println(divider)
	}
}

type StdoutWriter struct {
	output js.Value
}

func (w *StdoutWriter) Write(p []byte) (n int, err error) {
	w.output.Set("value", w.output.Get("value").String()+string(p))
	return len(p), nil
}

func Run() {
	input := js.Global().Get("document").Call("getElementById", "code")
	// output := js.Global().Get("document").Call("getElementById", "output")

	output := js.Global().Get("document").Call("getElementById", "output")
	stdout := &StdoutWriter{output}

	input.Call("addEventListener", "blur", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		output.Set("value", "")
		code := input.Get("value").String()

		prog, err := parser.ParseProgram([]byte(code))
		if err != nil {
			js.Global().Get("alert").Invoke(err.Error())
		}

		_, err = interpreter.Execute(prog, &interpreter.Config{Args: []string{}, Stdout: stdout})

		if err != nil {
			errorMessage := fmt.Sprintf("%s", err)
			if e, ok := err.(interpreter.Error); ok {
				showErrorSource([]byte(code), e.Position(), len(errorMessage))
			}
		}

		return nil
	}))

	<-make(chan int)
}
